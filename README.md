# MNT Reform RCORE RK3588 SoM

![MNT Reform RCORE RK3588 SoM Render](images/reform-rcore-rk3588-render.png)
![MNT Reform RCORE RK3588 SoM Render](images/reform-rcore-rk3588-layers.png)
![MNT Reform RCORE RK3588 Photo](images/mnt-reform-rcore-rk3588-processor-module.jpg)

This is an open hardware adapter module that enables the use of Firefly iCore RK3588Q system-on-modules in the MNT Reform family of open hardware laptops. It was designed in KiCAD by MNT Research GmbH.

## Status

The first development version of the module runs a full Debian Linux desktop and is validated and integrated with MNT Reform and is the first module made with the requirements of MNT Reform Next in mind. Thanks to the great work of Collabora all necessary peripherals work, and the open source Panthor driver was recently merged in Mesa, enabling OpenGL 3.1.

## Specs

- System-on-Chip: Rockchip RK3588
- CPU: 4x ARM Cortex-A76 and 4x ARM Cortex-A55
- GPU: ARM Mali G610 MP4
- Memory: 16GB or 32GB LPDDR4
- 1x PCIe 3.0 with 4 lanes (routed to M.2 slot on MNT Reform)
- 1x PCIe 2.0 with 1 lane (routed to mPCIe slot on MNT Reform)
- 2x USB 3.0+2.0
- 1GBit Ethernet w/ Microchip KSZ9310 PHY
- SAI audio
- On the iCore module: 128GB or 256GB eMMC Flash
- SD, UART, SPI, I2C, PWM, GPIO
- Powered by single 5V input

## TBD

- DSI / eDP (implemented in hardware, but no drivers in mainline yet)
- SPI (next revision)

## Firmware/Software

- For sources, see `mkimage.sh`: https://source.mnt.re/reform/reform-system-image

## License

All hardware sources are copyright 2024 MNT Research GmbH and licensed under the [CERN Open Hardware Licence Version 2 - Strongly Reciprocal](https://ohwr.org/project/cernohl/wikis/uploads/002d0b7d5066e6b3829168730237bddb/cern_ohl_s_v2.txt).

## Funding

![NLNet logo](images/nlnet-320x120.png)

This project [receives funding by NLNet](https://nlnet.nl/project/MNT-Reform-Next/).
