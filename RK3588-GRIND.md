# RK3588 GRIND

## minimum needed for MVP

- [x] Console UART
  - [x] UART2 TX/RX
  - [x] (1.8V at rk3588, shift)
- [x] RESET line (NPOR)
  - [x] shift
- [x] PWRON_L (LPC?) -> 1v8 pullup for now
- [x] HDMI
  - [x] we don't have support for anything besides HDMI in the kernel, so put HDMI on header like RCM4
    - [x] wire up HDMI0 (we call it HDMI2)
    - [x] 220nF caps
    - [x] clk pair correct? aha, CLK is TX3
    - [x] FRL circuit
    - [x] HPD
    - [x] DDC level shifting
    - [x] HPD protection/level shift
  - [x] connect HDMI_TX1 to the outside
    - [x] FRL circuit
    - [x] HPD
    - [x] DDC/CEC and level shift
    - [x] HPD protection/level shift
- [x] DSI
  - [x] connector
- [x] eDP -> lets reuse the same connector as HDMI0?
  - [x] backlight EN
  - [x] backlight PWM
- [x] USB 2.0 ports
- [x] USB 3.0 ports
  -  [x] connect so that maskrom usb boot bypasses usb hub on mobo
- [x] Ethernet PHY
  - [x] ETH_RST_N
- [x] SD card
- [x] 2x PCIe
  - [x] pcie20 to pcie1 (refclk output)
  - [x] pcie30 makes most sense for nvme, pcie2 (refclk input)
- [x] audio (I2S)

- [x] I2C for audio, rtc, edp
- [x] USB hub reset -> choose a gpio

## powers

- [x] 4V
- [x] 1.2V
- [x] 1.8V (use from module)
- [x] 3.3V (use from module)

### unsolved (rev2?)

- [ ] WIFI / BT?? -> wifi is ampak AP6275PR3 on eval board. squeeze it under the SOM?
- [ ] LPC SPI
- [ ] phone audio (secondary i2s)
