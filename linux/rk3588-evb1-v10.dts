// SPDX-License-Identifier: (GPL-2.0+ OR MIT)
/*
 * Copyright (c) 2021 Rockchip Electronics Co., Ltd.
 *
 */

/dts-v1/;

#include <dt-bindings/gpio/gpio.h>
#include <dt-bindings/input/input.h>
#include <dt-bindings/pinctrl/rockchip.h>
#include <dt-bindings/usb/pd.h>
#include <dt-bindings/display/rockchip_vop.h>
#include "rk3588.dtsi"

#define PCA_IO0_0          0
#define PCA_IO0_1          1
#define PCA_IO0_2          2
#define PCA_IO0_3          3
#define PCA_IO0_4          4
#define PCA_IO0_5          5
#define PCA_IO0_6          6
#define PCA_IO0_7          7
#define PCA_IO1_0          8
#define PCA_IO1_1          9
#define PCA_IO1_2          10
#define PCA_IO1_3          11
#define PCA_IO1_4          12
#define PCA_IO1_5          13
#define PCA_IO1_6          14
#define PCA_IO1_7          15

/ {
	model = "Rockchip RK3588 EVB1 V10 Board";
	compatible = "rockchip,rk3588-evb1-v10", "rockchip,rk3588";

	aliases {
		mmc0 = &sdhci;
		serial2 = &uart2;
	};

	chosen {
    bootargs = "";
		stdout-path = "serial2:1500000n8";
	};

	vcc12v_dcin: vcc12v-dcin-regulator {
		compatible = "regulator-fixed";
		regulator-name = "vcc12v_dcin";
		regulator-always-on;
		regulator-boot-on;
		regulator-min-microvolt = <12000000>;
		regulator-max-microvolt = <12000000>;
	};

	vcc5v0_sys: vcc5v0-sys {
		compatible = "regulator-fixed";
		regulator-name = "vcc5v0_sys";
		regulator-always-on;
		regulator-boot-on;
		regulator-min-microvolt = <5000000>;
		regulator-max-microvolt = <5000000>;
		vin-supply = <&vcc12v_dcin>;
	};

	vcc5v0_usbdcin: vcc5v0-usbdcin {
		compatible = "regulator-fixed";
		regulator-name = "vcc5v0_usbdcin";
		regulator-always-on;
		regulator-boot-on;
		regulator-min-microvolt = <5000000>;
		regulator-max-microvolt = <5000000>;
		vin-supply = <&vcc12v_dcin>;
	};

	vcc5v0_usb: vcc5v0-usb {
		compatible = "regulator-fixed";
		regulator-name = "vcc5v0_usb";
		regulator-always-on;
		regulator-boot-on;
		regulator-min-microvolt = <5000000>;
		regulator-max-microvolt = <5000000>;
		vin-supply = <&vcc12v_dcin>;
	};

	vcc_1v1_nldo_s3: vcc-1v1-nldo-s3 {
		compatible = "regulator-fixed";
		regulator-name = "vcc_1v1_nldo_s3";
		regulator-always-on;
		regulator-boot-on;
		regulator-min-microvolt = <1100000>;
		regulator-max-microvolt = <1100000>;
		vin-supply = <&vcc5v0_sys>;
	};

  /* --- from "port" dtsi --- */

  vcc5v0_host: vcc5v0-host {
		compatible = "regulator-fixed";
		regulator-name = "vcc5v0_host";
		regulator-boot-on;
		regulator-always-on;
		regulator-min-microvolt = <5000000>;
		regulator-max-microvolt = <5000000>;
		enable-active-high;
		gpio = <&pca9555 PCA_IO0_5 GPIO_ACTIVE_HIGH>;
		vin-supply = <&vcc5v0_usb>;
		status = "okay";
	};

	/*vcc_hub_reset: vcc-hub-reset-regulator {
		compatible = "regulator-fixed";
		regulator-name = "vcc_hub_reset";
		regulator-boot-on;
		regulator-always-on;
		enable-active-high;
		status = "okay";
		gpio = <&pca9555 PCA_IO0_4 GPIO_ACTIVE_HIGH>;  //PCA_IO 04
	};*/

	vbus5v0_typec_pwr_en: vbus5v0-typec-pwr-en-regulator {
		compatible = "regulator-fixed";
		regulator-name = "vbus5v0_typec_pwr_en";
		enable-active-high;
		status = "okay";
		gpio = <&pca9555 PCA_IO1_4 GPIO_ACTIVE_HIGH>;  //PCA_IO 14
	};

  /* --- from "diff" dtsi --- */

  /* TODO: firefly has some special code here */
  /*vcc_hub3_reset: vcc-hub3-reset-regulator {
		compatible = "regulator-fixed";
		regulator-name = "vcc_hub3_reset";
		regulator-always-on;
		enable-active-high;
		status = "okay";
		gpio = <&pca9555 PCA_IO0_6 GPIO_ACTIVE_HIGH>;  //PCA_IO 06

	  regulator-fixed-kernel-reset-ms = <3000>;
	};*/

	vcc5v0_host3: vcc5v0-host3 {
		compatible = "regulator-fixed";
		regulator-name = "vcc5v0_host3";
		regulator-boot-on;
		regulator-always-on;
		regulator-min-microvolt = <5000000>;
		regulator-max-microvolt = <5000000>;
		enable-active-high;

		gpio = <&pca9555 PCA_IO0_7 GPIO_ACTIVE_HIGH>;

		vin-supply = <&vcc5v0_usb>;
		status = "okay";
	};

	vcc_sata_pwr_en: vcc-sata-pwr-en-regulator {
		compatible = "regulator-fixed";
		regulator-name = "vcc_sata_pwr_en";
		regulator-boot-on;
		regulator-always-on;
		enable-active-high;
		status = "disabled";
		gpio = <&pca9555 PCA_IO1_2 GPIO_ACTIVE_HIGH>;  //PCA_IO 12
	};

	vcc_fan_pwr_en: vcc-fan-pwr-en-regulator {
		compatible = "regulator-fixed";
		regulator-name = "vcc_fan_pwr_en";
		regulator-boot-on;
		regulator-always-on;
		enable-active-high;
		status = "disabled";
		gpio = <&pca9555 PCA_IO1_3 GPIO_ACTIVE_HIGH>;  //PCA_IO 13
	};

	vcc_sdcard_pwr_en: vcc-sdcard-pwr-en-regulator {
		compatible = "regulator-fixed";
		regulator-name = "vcc_sdcard_pwr_en";
		regulator-boot-on;
		regulator-always-on;
		enable-active-high;
		gpio = <&pca9555 PCA_IO1_5 GPIO_ACTIVE_HIGH>;  //PCA_IO 15
		status = "okay";
	};

	vcc3v3_pcie30: vcc3v3-pcie30 {
		compatible = "regulator-fixed";
		regulator-name = "vcc3v3_pcie30";
		regulator-min-microvolt = <3300000>;
		regulator-max-microvolt = <3300000>;
		enable-active-high;
		gpios = <&gpio2 RK_PC5 GPIO_ACTIVE_HIGH>;
		startup-delay-us = <5000>;
		vin-supply = <&vcc12v_dcin>;
	};

	pcie30_avdd1v8: pcie30-avdd1v8 {
		compatible = "regulator-fixed";
		regulator-name = "pcie30_avdd1v8";
		regulator-boot-on;
		regulator-always-on;
		regulator-min-microvolt = <1800000>;
		regulator-max-microvolt = <1800000>;
		vin-supply = <&avcc_1v8_s0>;
	};

	pcie30_avdd0v75: pcie30-avdd0v75 {
		compatible = "regulator-fixed";
		regulator-name = "pcie30_avdd0v75";
		regulator-boot-on;
		regulator-always-on;
		regulator-min-microvolt = <750000>;
		regulator-max-microvolt = <750000>;
		vin-supply = <&avdd_0v75_s0>;
	};


};

&cpu_b0 {
	cpu-supply = <&vdd_cpu_big0_s0>;
	mem-supply = <&vdd_cpu_big0_mem_s0>;
};

&cpu_b1 {
	cpu-supply = <&vdd_cpu_big0_s0>;
	mem-supply = <&vdd_cpu_big0_mem_s0>;
};

&cpu_b2 {
	cpu-supply = <&vdd_cpu_big1_s0>;
	mem-supply = <&vdd_cpu_big1_mem_s0>;
};

&cpu_b3 {
	cpu-supply = <&vdd_cpu_big1_s0>;
	mem-supply = <&vdd_cpu_big1_mem_s0>;
};

&cpu_l0 {
	cpu-supply = <&vdd_cpu_lit_s0>;
	mem-supply = <&vdd_cpu_lit_mem_s0>;
};

&cpu_l1 {
	cpu-supply = <&vdd_cpu_lit_s0>;
	mem-supply = <&vdd_cpu_lit_mem_s0>;
};

&cpu_l2 {
	cpu-supply = <&vdd_cpu_lit_s0>;
	mem-supply = <&vdd_cpu_lit_mem_s0>;
};

&cpu_l3 {
	cpu-supply = <&vdd_cpu_lit_s0>;
	mem-supply = <&vdd_cpu_lit_mem_s0>;
};

&gpu {
	mali-supply = <&vdd_gpu_s0>;
	sram-supply = <&vdd_gpu_mem_s0>;
	status = "okay";
};

&cpu_b0 {
	cpu-supply = <&vdd_cpu_big0_s0>;
	mem-supply = <&vdd_cpu_big0_s0>;
};

&cpu_b1 {
	cpu-supply = <&vdd_cpu_big0_s0>;
	mem-supply = <&vdd_cpu_big0_s0>;
};

&cpu_b2 {
	cpu-supply = <&vdd_cpu_big1_s0>;
	mem-supply = <&vdd_cpu_big1_s0>;
};

&cpu_b3 {
	cpu-supply = <&vdd_cpu_big1_s0>;
	mem-supply = <&vdd_cpu_big1_s0>;
};

&cpu_l0 {
	cpu-supply = <&vdd_cpu_lit_s0>;
	mem-supply = <&vdd_cpu_lit_mem_s0>;
};

&cpu_l1 {
	cpu-supply = <&vdd_cpu_lit_s0>;
	mem-supply = <&vdd_cpu_lit_mem_s0>;
};

&cpu_l2 {
	cpu-supply = <&vdd_cpu_lit_s0>;
	mem-supply = <&vdd_cpu_lit_mem_s0>;
};

&cpu_l3 {
	cpu-supply = <&vdd_cpu_lit_s0>;
	mem-supply = <&vdd_cpu_lit_mem_s0>;
};

&i2c0 {
	pinctrl-names = "default";
	pinctrl-0 = <&i2c0m2_xfer>; // TODO: check
	status = "okay";

	vdd_cpu_big0_s0: vdd_cpu_big0_mem_s0: rk8602@42 {
		compatible = "rockchip,rk8602";
		reg = <0x42>;

		vin-supply = <&vcc5v0_sys>;
		fcs,suspend-voltage-selector = <1>;
		rockchip,suspend-voltage-selector = <1>;

		regulator-compatible = "rk860x-reg";
		regulator-name = "vdd_cpu_big0_s0";
		regulator-min-microvolt = <0x86470>;
		regulator-max-microvolt = <0x100590>;
		regulator-ramp-delay = <0x8fc>;
		regulator-boot-on;
		regulator-always-on;

		regulator-state-mem {
			regulator-off-in-suspend;
		};
  };

	vdd_cpu_big1_s0: vdd_cpu_big1_mem_s0: rk8603@43 {
		compatible = "rockchip,rk8602", "rockchip,rk8603";
		reg = <0x43>;

		vin-supply = <&vcc5v0_sys>;
		fcs,suspend-voltage-selector = <1>;
		rockchip,suspend-voltage-selector = <1>;

		regulator-compatible = "rk860x-reg";
		regulator-name = "vdd_cpu_big1_s0";
		regulator-min-microvolt = <550000>;
		regulator-max-microvolt = <1050000>;
		regulator-ramp-delay = <2300>;
		regulator-boot-on;
		regulator-always-on;

		regulator-state-mem {
			regulator-off-in-suspend;
		};
  };
};

&i2c1 {
  pinctrl-names = "default";
	pinctrl-0 = <&i2c1m2_xfer>; // TODO: check
	status = "okay";

	vdd_npu_s0: vdd_npu_mem_s0: regulator@42 {
		compatible = "rockchip,rk8602";
		reg = <0x42>;
		fcs,suspend-voltage-selector = <1>;
		regulator-name = "vdd_npu_s0";
		regulator-always-on;
		regulator-boot-on;
		regulator-min-microvolt = <550000>;
		regulator-max-microvolt = <950000>;
		regulator-ramp-delay = <2300>;
		vin-supply = <&vcc5v0_sys>;

		regulator-state-mem {
			regulator-off-in-suspend;
		};
	};
};

&i2c6 {
	status = "okay";
	pinctrl-names = "default";
	pinctrl-0 = <&i2c6m0_xfer>;

	pca9555: gpio@21 {
		compatible = "nxp,pca9555";
		reg = <0x21>;
		gpio-controller;
		#gpio-cells = <0x02>;
    gpio-group-num = <300>;
		status = "okay";
	};

	pca9555_1: gpio@20 {
		compatible = "nxp,pca9555";
		reg = <0x20>;
		gpio-controller;
		#gpio-cells = <0x02>;
    gpio-group-num = <300>;
		status = "okay";
	};

	hym8563: rtc@51 {
		compatible = "haoyu,hym8563";
		reg = <0x51>;
		#clock-cells = <0>;
		clock-output-names = "hym8563";
		pinctrl-names = "default";
		pinctrl-0 = <&hym8563_int>;
		//interrupt-parent = <&gpio0>;
		//interrupts = <RK_PD4 IRQ_TYPE_LEVEL_LOW>;

	  interrupt-parent = <&gpio0>;
	  interrupts = <RK_PB0 IRQ_TYPE_LEVEL_LOW>;
		wakeup-source;
	};

  /* TODO: fusb */
};

&i2c3 {
	status = "okay";
};

&pwm2 {
	status = "okay";
};

&saradc {
	vref-supply = <&avcc_1v8_s0>;
	status = "okay";
};

&sdhci {
	bus-width = <8>;
	no-sdio;
	no-sd;
	non-removable;
	max-frequency = <200000000>;
	mmc-hs400-1_8v;
	mmc-hs400-enhanced-strobe;
	status = "okay";
};

&sdmmc {
	bus-width = <4>;
	cap-mmc-highspeed;
	cap-sd-highspeed;
	disable-wp;
	max-frequency = <150000000>;
	no-sdio;
	no-mmc;
	sd-uhs-sdr104;
	vqmmc-supply = <&vccio_sd_s0>;
	status = "disabled";
};

&spi2 {
	status = "okay";
	assigned-clocks = <&cru CLK_SPI2>;
	assigned-clock-rates = <200000000>;
	pinctrl-names = "default";
	pinctrl-0 = <&spi2m2_cs0 &spi2m2_pins>;
	num-cs = <1>;

	rk806single: pmic@0 {
		compatible = "rockchip,rk806";
		spi-max-frequency = <1000000>;
		reg = <0x0>;

		interrupt-parent = <&gpio0>;
		interrupts = <7 IRQ_TYPE_LEVEL_LOW>;

		pinctrl-names = "default", "pmic-power-off";
		pinctrl-0 = <&pmic_pins>, <&rk806_dvs1_null>,
			    <&rk806_dvs2_null>, <&rk806_dvs3_null>;
		pinctrl-1 = <&rk806_dvs1_pwrdn>;

    /* TODO: missing some thresholds */

		pmic-reset-func = <1>;

		vcc1-supply = <&vcc5v0_sys>;
		vcc2-supply = <&vcc5v0_sys>;
		vcc3-supply = <&vcc5v0_sys>;
		vcc4-supply = <&vcc5v0_sys>;
		vcc5-supply = <&vcc5v0_sys>;
		vcc6-supply = <&vcc5v0_sys>;
		vcc7-supply = <&vcc5v0_sys>;
		vcc8-supply = <&vcc5v0_sys>;
		vcc9-supply = <&vcc5v0_sys>;
		vcc10-supply = <&vcc5v0_sys>;
		vcc11-supply = <&vcc_2v0_pldo_s3>;
		vcc12-supply = <&vcc5v0_sys>;
		vcc13-supply = <&vcc_1v1_nldo_s3>;
		vcc14-supply = <&vcc_1v1_nldo_s3>;
		vcca-supply = <&vcc5v0_sys>;

		#gpio-cells = <2>;
		gpio-controller;

		rk806_dvs1_null: dvs1-null-pins {
			pins = "gpio_pwrctrl2";
			function = "pin_fun0";
		};

		rk806_dvs1_slp: rk806_dvs1_slp {
			pins = "gpio_pwrctrl1";
			function = "pin_fun1";
		};

    rk806_dvs1_pwrdn: rk806_dvs1_pwrdn {
			pins = "gpio_pwrctrl1";
			function = "pin_fun2";
		};

		rk806_dvs1_rst: rk806_dvs1_rst {
			pins = "gpio_pwrctrl1";
			function = "pin_fun3";
		};

		rk806_dvs2_null: rk806_dvs2_null {
			pins = "gpio_pwrctrl2";
			function = "pin_fun0";
		};

		rk806_dvs2_slp: rk806_dvs2_slp {
			pins = "gpio_pwrctrl2";
			function = "pin_fun1";
		};

		rk806_dvs2_pwrdn: rk806_dvs2_pwrdn {
			pins = "gpio_pwrctrl2";
			function = "pin_fun2";
		};

		rk806_dvs2_rst: rk806_dvs2_rst {
			pins = "gpio_pwrctrl2";
			function = "pin_fun3";
		};

		rk806_dvs2_dvs: rk806_dvs2_dvs {
			pins = "gpio_pwrctrl2";
			function = "pin_fun4";
		};

		rk806_dvs2_gpio: rk806_dvs2_gpio {
			pins = "gpio_pwrctrl2";
			function = "pin_fun5";
		};

		rk806_dvs3_null: rk806_dvs3_null {
			pins = "gpio_pwrctrl3";
			function = "pin_fun0";
		};

		rk806_dvs3_slp: rk806_dvs3_slp {
			pins = "gpio_pwrctrl3";
			function = "pin_fun1";
		};

		rk806_dvs3_pwrdn: rk806_dvs3_pwrdn {
			pins = "gpio_pwrctrl3";
			function = "pin_fun2";
		};

		rk806_dvs3_rst: rk806_dvs3_rst {
			pins = "gpio_pwrctrl3";
			function = "pin_fun3";
		};

		rk806_dvs3_dvs: rk806_dvs3_dvs {
			pins = "gpio_pwrctrl3";
			function = "pin_fun4";
		};

		rk806_dvs3_gpio: rk806_dvs3_gpio {
			pins = "gpio_pwrctrl3";
			function = "pin_fun5";
		};

		regulators {
			vdd_gpu_s0: vdd_gpu_mem_s0: dcdc-reg1 {
				regulator-name = "vdd_gpu_s0";
				regulator-always-on;
				regulator-boot-on;
				regulator-min-microvolt = <550000>;
				regulator-max-microvolt = <950000>;
				regulator-ramp-delay = <12500>;
				regulator-enable-ramp-delay = <400>;
				regulator-state-mem {
					regulator-off-in-suspend;
				};
			};

			vdd_cpu_lit_s0: vdd_cpu_lit_mem_s0: dcdc-reg2 {
				regulator-name = "vdd_cpu_lit_s0";
				regulator-always-on;
				regulator-boot-on;
				regulator-min-microvolt = <550000>;
				regulator-max-microvolt = <950000>;
				regulator-ramp-delay = <12500>;
				regulator-state-mem {
					regulator-off-in-suspend;
				};
			};

			vdd_log_s0: dcdc-reg3 {
				regulator-name = "vdd_log_s0";
				regulator-always-on;
				regulator-boot-on;
				regulator-min-microvolt = <675000>;
				regulator-max-microvolt = <750000>;
				regulator-ramp-delay = <12500>;
				regulator-state-mem {
					regulator-off-in-suspend;
					regulator-suspend-microvolt = <750000>;
				};
			};

			vdd_vdenc_s0: dcdc-reg4 {
				regulator-name = "vdd_vdenc_s0";
				regulator-always-on;
				regulator-boot-on;
				regulator-min-microvolt = <550000>;
				regulator-max-microvolt = <950000>;
				regulator-init-microvolt = <750000>;
				regulator-ramp-delay = <12500>;
				regulator-state-mem {
					regulator-off-in-suspend;
				};

			};

			vdd_ddr_s0: dcdc-reg5 {
				regulator-name = "vdd_ddr_s0";
				regulator-always-on;
				regulator-boot-on;
				regulator-min-microvolt = <675000>;
				regulator-max-microvolt = <950000>;
				regulator-ramp-delay = <12500>;
				regulator-state-mem {
					regulator-off-in-suspend;
					regulator-suspend-microvolt = <850000>;
				};

			};

			vdd2_ddr_s3: dcdc-reg6 {
				regulator-name = "vdd2_ddr_s3";
				regulator-always-on;
				regulator-boot-on;

				regulator-state-mem {
					regulator-on-in-suspend;
				};
			};

			vcc_2v0_pldo_s3: dcdc-reg7 {
				regulator-always-on;
				regulator-boot-on;
				regulator-min-microvolt = <2000000>;
				regulator-max-microvolt = <2000000>;
				regulator-name = "vdd_2v0_pldo_s3";
				regulator-state-mem {
					regulator-on-in-suspend;
					regulator-suspend-microvolt = <2000000>;
				};
			};

			vcc_3v3_s3: dcdc-reg8 {
				regulator-name = "vcc_3v3_s3";
				regulator-always-on;
				regulator-boot-on;
				regulator-min-microvolt = <3300000>;
				regulator-max-microvolt = <3300000>;

				regulator-state-mem {
					regulator-on-in-suspend;
					regulator-suspend-microvolt = <3300000>;
				};
			};

			vddq_ddr_s0: dcdc-reg9 {
				regulator-name = "vddq_ddr_s0";
				regulator-always-on;
				regulator-boot-on;

				regulator-state-mem {
					regulator-off-in-suspend;
				};
			};

			vcc_1v8_s3: dcdc-reg10 {
				regulator-name = "vcc_1v8_s3";
				regulator-always-on;
				regulator-boot-on;
				regulator-min-microvolt = <1800000>;
				regulator-max-microvolt = <1800000>;

				regulator-state-mem {
					regulator-on-in-suspend;
					regulator-suspend-microvolt = <1800000>;
				};
			};

			avcc_1v8_s0: pldo-reg1 {
				regulator-always-on;
				regulator-boot-on;
				regulator-min-microvolt = <1800000>;
				regulator-max-microvolt = <1800000>;
				regulator-name = "avcc_1v8_s0";
				regulator-state-mem {
					regulator-off-in-suspend;
				};
			};

			vcc_1v8_s0: pldo-reg2 {
				regulator-name = "vcc_1v8_s0";
				regulator-always-on;
				regulator-boot-on;
				regulator-min-microvolt = <1800000>;
				regulator-max-microvolt = <1800000>;

				regulator-state-mem {
					regulator-off-in-suspend;
					regulator-suspend-microvolt = <1800000>;
				};
			};

			avdd_1v2_s0: pldo-reg3 {
				regulator-name = "avdd_1v2_s0";
				regulator-always-on;
				regulator-boot-on;
				regulator-min-microvolt = <1200000>;
				regulator-max-microvolt = <1200000>;

				regulator-state-mem {
					regulator-off-in-suspend;
				};
			};

			vcc_3v3_s0: pldo-reg4 {
				regulator-name = "vcc_3v3_s0";
				regulator-always-on;
				regulator-boot-on;
				regulator-min-microvolt = <3300000>;
				regulator-max-microvolt = <3300000>;
				regulator-state-mem {
					regulator-on-in-suspend;
				};
			};

			vccio_sd_s0: pldo-reg5 {
				regulator-name = "vccio_sd_s0";
				regulator-always-on;
				regulator-boot-on;
				regulator-min-microvolt = <1800000>;
				regulator-max-microvolt = <3300000>;
				regulator-state-mem {
					regulator-off-in-suspend;
				};
			};

			pldo6_s3: pldo-reg6 {
				regulator-name = "pldo6_s3";
				regulator-always-on;
				regulator-boot-on;
				regulator-min-microvolt = <1800000>;
				regulator-max-microvolt = <1800000>;

				regulator-state-mem {
					regulator-on-in-suspend;
					regulator-suspend-microvolt = <1800000>;
				};
			};

			vdd_0v75_s3: nldo-reg1 {
				regulator-name = "vdd_0v75_s3";
				regulator-always-on;
				regulator-boot-on;
				regulator-min-microvolt = <750000>;
				regulator-max-microvolt = <750000>;

				regulator-state-mem {
					regulator-on-in-suspend;
					regulator-suspend-microvolt = <750000>;
				};
			};

			vdd_ddr_pll_s0: nldo-reg2 {
				regulator-name = "vdd_ddr_pll_s0";
				regulator-always-on;
				regulator-boot-on;
				regulator-min-microvolt = <850000>;
				regulator-max-microvolt = <850000>;

				regulator-state-mem {
					regulator-off-in-suspend;
					regulator-suspend-microvolt = <850000>;
				};
			};

			avdd_0v75_s0: nldo-reg3 {
				regulator-name = "avdd_0v75_s0";
				regulator-always-on;
				regulator-boot-on;
				regulator-min-microvolt = <750000>;
				regulator-max-microvolt = <750000>;

				regulator-state-mem {
					regulator-off-in-suspend;
				};
			};

			vdd_0v85_s0: nldo-reg4 {
				regulator-name = "vdd_0v85_s0";
				regulator-always-on;
				regulator-boot-on;
				regulator-min-microvolt = <850000>;
				regulator-max-microvolt = <850000>;

				regulator-state-mem {
					regulator-off-in-suspend;
				};
			};

			vdd_0v75_s0: nldo-reg5 {
				regulator-always-on;
				regulator-boot-on;
				regulator-min-microvolt = <750000>;
				regulator-max-microvolt = <750000>;
				regulator-name = "vdd_0v75_s0";
				regulator-state-mem {
					regulator-off-in-suspend;
				};
			};
		};
	};
};

&uart2 {
	pinctrl-0 = <&uart2m0_xfer>; // TODO check
	status = "okay";
};

/* ---- peripherals ---- */

&gmac0 {
	clock_in_out = "output";
	phy-handle = <&rgmii_phy>;
	phy-mode = "rgmii-rxid";
	pinctrl-0 = <&gmac0_miim
		     &gmac0_tx_bus2
		     &gmac0_rx_bus2
		     &gmac0_rgmii_clk
		     &gmac0_rgmii_bus>;
	pinctrl-names = "default";
	rx_delay = <0x00>;
	tx_delay = <0x47>;
	snps,reset-gpio = <&gpio3 RK_PC7 GPIO_ACTIVE_LOW>;
	snps,reset-active-low;
	snps,reset-delays-us = <0 20000 100000>;
	status = "okay";
	mac-address = [ fe 46 f2 84 84 fe ];
};

&gmac0_tx_bus2 {
  rockchip,pins =
    /* gmac0_txd0 */
    <2 RK_PB6 1 &pcfg_pull_up_drv_level_6>,
    /* gmac0_txd1 */
    <2 RK_PB7 1 &pcfg_pull_up_drv_level_6>,
    /* gmac0_txen */
    <2 RK_PC0 1 &pcfg_pull_none>;
};

&gmac0_rgmii_bus {
	rockchip,pins =
	/* gmac0_rxd2 */
	<2 RK_PA6 1 &pcfg_pull_none>,
	/* gmac0_rxd3 */
	<2 RK_PA7 1 &pcfg_pull_none>,
	/* gmac0_txd2 */
	<2 RK_PB1 1 &pcfg_pull_up_drv_level_6>,
	/* gmac0_txd3 */
	<2 RK_PB2 1 &pcfg_pull_up_drv_level_6>;
};

&mdio0 {
	rgmii_phy: ethernet-phy@1 {
		compatible = "ethernet-phy-ieee802.3-c22";
		reg = <0x1>;
	};
};

/* --- usb --- */


&u2phy0_otg {
	rockchip,typec-vbus-det;
};

&u2phy2_host {
	phy-supply = <&vcc5v0_host>;
};

&u2phy3_host {
	phy-supply = <&vcc5v0_host>;
};

&combphy1_ps {
  status = "okay";
};

&usbdp_phy1 {
  status = "okay";
};

&u2phy1 {
  status = "okay";
};

&u2phy1_otg {
  status = "okay";
};

&usbdp_phy1_u3 {
  status = "okay";
};

/*usb3.1 host2 controller for 5G module*/
/*usb_host2_xhci {
  dr_mode = "host";
  status = "okay";
};*/

&combphy2_psu {
  status = "okay"; // USB3.1/SATA/PCIe Combo PHY
};

&u2phy0 {
	status = "okay";
};

&u2phy2 {
	status = "okay";
};

&u2phy3 {
	status = "okay";
};

&u2phy0_otg {
	status = "okay";
};

&u2phy2_host {
	status = "okay";
};

&u2phy3_host {
	status = "okay";
};

&usb_host0_ehci {
	status = "okay";
};

&usb_host0_ohci {
	status = "okay";
};

&usb_host1_ehci {
	status = "okay";
};

&usb_host1_ohci {
	status = "okay";
};

&usbdp_phy0 {
	status = "okay";
};

&usbdp_phy0_dp {
	status = "okay";
};

&usbdp_phy0_u3 {
	status = "okay";
};

//&usbdrd3_0 {
&usb_host0_xhci {
	dr_mode = "otg";
	status = "okay";
};

// perhaps the one going to the big hub?
&usb_host1_xhci {
	dr_mode = "host";
	status = "okay";
};

/* --- pinctrls from aio --- */

&pinctrl {

	dp {
		dp1_hpd: dp1-hpd {
			 rockchip,pins = <1 RK_PA4 RK_FUNC_GPIO &pcfg_pull_none>;
		};
	};

	leds {
		led_power: led-power {
			rockchip,pins = <3 RK_PB2 RK_FUNC_GPIO &pcfg_pull_none>;
		};
 	};

	hdmirx {
		hdmirx_det: hdmirx-det {
			rockchip,pins = <1 RK_PD5 RK_FUNC_GPIO &pcfg_pull_up>;
		};
	};

	headphone {
		hp_det: hp-det {
			rockchip,pins = <1 RK_PC4 RK_FUNC_GPIO &pcfg_pull_none>;
		};
	};

	hym8563 {
		hym8563_int: hym8563-int {
			rockchip,pins = <0 RK_PB0 RK_FUNC_GPIO &pcfg_pull_up>;
		};
	};

	lcd {
		lcd_rst_gpio: lcd-rst-gpio {
			rockchip,pins = <2 RK_PB4 RK_FUNC_GPIO &pcfg_pull_none>;
		};
	};

	touch {
		touch_gpio: touch-gpio {
			rockchip,pins =
				<0 RK_PD5 RK_FUNC_GPIO &pcfg_pull_up>,
				<0 RK_PC6 RK_FUNC_GPIO &pcfg_pull_none>;
		};
	};

	usb-typec {
		usbc0_int: usbc0-int {
			rockchip,pins = <0 RK_PD3 RK_FUNC_GPIO &pcfg_pull_up>;
		};
	};

	wireless-bluetooth {
		uart6_gpios: uart6-gpios {
			rockchip,pins = <1 RK_PA2 RK_FUNC_GPIO &pcfg_pull_none>;
		};

		bt_reset_gpio: bt-reset-gpio {
			rockchip,pins = <0 RK_PC6 RK_FUNC_GPIO &pcfg_pull_none>;
		};

		bt_wake_gpio: bt-wake-gpio {
			rockchip,pins = <0 RK_PC5 RK_FUNC_GPIO &pcfg_pull_none>;
		};

		bt_irq_gpio: bt-irq-gpio {
			rockchip,pins = <0 RK_PA0 RK_FUNC_GPIO &pcfg_pull_down>;
		};
	};

	wireless-wlan {
		wifi_host_wake_irq: wifi-host-wake-irq {
			rockchip,pins = <0 RK_PB2 RK_FUNC_GPIO &pcfg_pull_down>;
		};

		wifi_poweren_gpio: wifi-poweren-gpio {
			rockchip,pins = <0 RK_PC4 RK_FUNC_GPIO &pcfg_pull_up>;
		};
	};
};

/* --- hdmi --- */

// TODO

&hdmi0 {
	enable-gpios = <&gpio4 RK_PA0 GPIO_ACTIVE_HIGH>;
	status = "okay";
};

&hdmi0_in_vp0 {
	status = "okay";
};

&hdptxphy_hdmi0 {
	status = "okay";
};

&route_hdmi0 {
	status = "okay";
	connect = <&vp0_out_hdmi0>;

    /*
	/delete-property/ force-output;
	/delete-node/ force_timing;*/
};

&vop_mmu {
  status = "okay";
};

&vop {
  status = "okay";
};

&vp0 {
	rockchip,plane-mask = <(1 << ROCKCHIP_VOP2_CLUSTER0 | 1 << ROCKCHIP_VOP2_ESMART0)>;
	rockchip,primary-plane = <ROCKCHIP_VOP2_ESMART0>;
};

&vp1 {
	rockchip,plane-mask = <(1 << ROCKCHIP_VOP2_CLUSTER1 | 1 << ROCKCHIP_VOP2_ESMART1)>;
	rockchip,primary-plane = <ROCKCHIP_VOP2_ESMART1>;
};

&vp2 {
	rockchip,plane-mask = <(1 << ROCKCHIP_VOP2_CLUSTER2 | 1 << ROCKCHIP_VOP2_ESMART2)>;
	rockchip,primary-plane = <ROCKCHIP_VOP2_ESMART2>;
};

&vp3 {
	rockchip,plane-mask = <(1 << ROCKCHIP_VOP2_CLUSTER3 | 1 << ROCKCHIP_VOP2_ESMART3)>;
	rockchip,primary-plane = <ROCKCHIP_VOP2_ESMART3>;
};
